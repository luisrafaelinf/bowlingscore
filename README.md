# Bowling Scoring

To build and run the project you need to have Java 8+ and Maven 3.6+ installed in your system. After the build, a `BowlingScore/target/` directory is generated for you with a jar file named **BowlingScore.jar** in it. 
Run the jar with the nexts commands:



``` shell
# Clone the source code.
git clone https://luisrafaelinf@bitbucket.org/luisrafaelinf/bowlingscore.git

# Enter the project directory.
cd BowlingScore

# Compile the project.
mvn clean package

# Compile the project including integration test.
mvn clean install

# Use the following commands to test the application with the included test files.
java -jar target/BowlingScoring.jar src/main/resources/two_player.txt
java -jar target/BowlingScoring.jar src/main/resources/fouls_score.txt
java -jar target/BowlingScoring.jar src/main/resources/one_player.txt
java -jar target/BowlingScoring.jar src/main/resources/perfect_scores.txt
java -jar target/BowlingScoring.jar src/main/resources/one_player.txt
java -jar target/BowlingScoring.jar src/main/resources/zero_score.txt


```
