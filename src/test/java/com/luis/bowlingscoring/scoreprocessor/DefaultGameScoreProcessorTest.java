package com.luis.bowlingscoring.scoreprocessor;

import com.luis.bowlingscoring.common.constant.RollAgrupation;
import com.luis.bowlingscoring.common.constant.RollRepresentation;
import com.luis.bowlingscoring.common.model.Frame;
import com.luis.bowlingscoring.common.model.Pinfall;
import com.luis.bowlingscoring.fakedata.FakeRolls;
import com.luis.bowlingscoring.common.model.Roll;
import com.luis.bowlingscoring.common.validator.DefaultRollValidator;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DefaultGameScoreProcessorTest {

    GameScoreProcessor processor = DefaultGameScoreProcessor.getInstance();

    public DefaultGameScoreProcessorTest() {
    }

    @Test
    public void testProcessPinFall() {

        List<Roll> rollsStrikes = FakeRolls.getRolls(RollRepresentation.MIN_ROLLS, RollRepresentation.MAX_VALUE_ROLL);
        List<Roll> rollsZero = FakeRolls.getRolls(RollRepresentation.MIN_ROLLS, "0");
        List<Roll> rollsFault = FakeRolls.getRolls(RollRepresentation.MAX_ROLLS - RollAgrupation.MAX, RollRepresentation.FAULT);
        List<Roll> rollsSpare = Arrays.asList(
                Roll.getInstance("6", DefaultRollValidator.getInstance()),
                Roll.getInstance("4", DefaultRollValidator.getInstance())
        );
        
        
        List<Pinfall> processStrikes = processor.processPinFall(rollsStrikes);
        long amountStrikes = processStrikes.stream()
                .filter(f -> f.getRoll().equals(RollRepresentation.STRIKE))
                .count();
        
        List<Pinfall> processZero = processor.processPinFall(rollsZero);        
        long amountZeros = processZero.stream()
                .filter(f -> f.getRoll().equals(RollRepresentation.ZERO))
                .count();

        List<Pinfall> processFault = processor.processPinFall(rollsFault);        
        long amountFault = processFault.stream()
                .filter(f -> f.getRoll().equals(RollRepresentation.FAULT))
                .count();

        List<Pinfall> processSpare = processor.processPinFall(rollsSpare);
        long amountSpare = processSpare.stream()
                .filter(f -> f.getRoll().equals(RollRepresentation.SPARE))
                .count();

        Assertions.assertEquals(amountStrikes, RollRepresentation.MIN_ROLLS);
        Assertions.assertEquals(amountZeros, RollRepresentation.MIN_ROLLS);
        Assertions.assertEquals(amountFault, RollRepresentation.MAX_ROLLS - RollAgrupation.MAX);
        Assertions.assertEquals(amountSpare, 1);

    }

    @Test
    public void testProcessFramScore() {

        List<Frame> listStrikes = processor.processScoreFrame(FakeRolls.getRolls(RollRepresentation.MIN_ROLLS, RollRepresentation.STRIKE));
        int amountStrikes = listStrikes.get(listStrikes.size() - 1).getAmount();

        List<Integer> listNormalGame = processor.processScoreFrame(FakeRolls.getNormalGame())
                .stream()
                .map(f -> f.getAmount())
                .collect(Collectors.toList());
        List<Integer> ListNormalGameExpected = Arrays.asList(17,30,37,57,77,105,123,131,151,170);

        Assertions.assertEquals(300, amountStrikes);
        Assertions.assertEquals(listNormalGame, ListNormalGameExpected);

    }

}
