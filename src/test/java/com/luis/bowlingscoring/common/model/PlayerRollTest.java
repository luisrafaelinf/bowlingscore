package com.luis.bowlingscoring.common.model;

import com.luis.bowlingscoring.common.exception.IllegalRollException;
import com.luis.bowlingscoring.common.fakedata.FakeRolls;
import com.luis.bowlingscoring.common.validator.DefaultPlayerRollValidator;
import java.util.Arrays;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PlayerRollTest {

    public PlayerRollTest() {
    }

    @Test
    public void testPlayerNameNull() {
        Exception exception = Assertions.assertThrows(NullPointerException.class, () -> {
            PlayerRoll.getinstance(null, Arrays.asList(), null);
        });

        String expectedMessage = "Name of players must not be null";
        String actualMessage = exception.getMessage();

        Assertions.assertEquals(actualMessage, expectedMessage);

    }

    @Test
    public void testRollsIllegals() {
        Exception exception = Assertions.assertThrows(IllegalRollException.class, () -> {
            PlayerRoll.getinstance("Sam", Arrays.asList(), DefaultPlayerRollValidator.getInstance());
        });

        String expectedMessage = "The total of rolls is not valid. "
                + "Please verify the file and fill it correctly.";
        String actualMessage = exception.getMessage();

        Assertions.assertEquals(actualMessage, expectedMessage);

    }

    @Test
    public void testRollInstanciate() {

        PlayerRoll instance = PlayerRoll.getinstance("Sam", FakeRolls.getRolls(21, "0"), DefaultPlayerRollValidator.getInstance());

        Assertions.assertNotNull(instance);

    }

}
