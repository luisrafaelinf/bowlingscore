
package com.luis.bowlingscoring.common.model;

import com.luis.bowlingscoring.common.exception.InvalidRollException;
import com.luis.bowlingscoring.common.validator.DefaultRollValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RollTest {

    public RollTest() {
    }

    @Test
    public void testRollScoreNull() {
        Exception exception = Assertions.assertThrows(NullPointerException.class, () -> {
            Roll.getInstance(null, null);
        });

        String expectedMessage = "Score must not be null";
        String actualMessage = exception.getMessage();

        Assertions.assertEquals(actualMessage, expectedMessage);

    }

    @Test
    public void testInvalidRoll() {
        Exception exception = Assertions.assertThrows(InvalidRollException.class, () -> {
            Roll.getInstance("12", DefaultRollValidator.getInstance());
        });

        String expectedMessage = "Roll score 12 invalid. Please verify again.";
        String actualMessage = exception.getMessage();

        Assertions.assertEquals(actualMessage, expectedMessage);

    }

    @Test
    public void testRollInstanciate() {

        Roll instance = Roll.getInstance("9", DefaultRollValidator.getInstance());

        Assertions.assertNotNull(instance);

    }

}
