
package com.luis.bowlingscoring.common.constant;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RollRepresentationTest {
    
    public RollRepresentationTest() {
    }
    
    @Test
    public void testRollRepresentationFault() {
        
        Assertions.assertEquals(RollRepresentation.FAULT, "F");
        
    }
    
    @Test
    public void testRollRepresentationMaxRolls() {
        
        Assertions.assertEquals(RollRepresentation.MAX_ROLLS, 24);
        
    }
    
    @Test
    public void testRollRepresentationMaxValueRoll() {
        
        Assertions.assertEquals(RollRepresentation.MAX_VALUE_ROLL, "10");
        
    }
    
    @Test
    public void testRollRepresentationMinRolls() {
        
        Assertions.assertEquals(RollRepresentation.MIN_ROLLS, 12);
        
    }
    
    @Test
    public void testRollRepresentationMinValueRolls() {
        
        Assertions.assertEquals(RollRepresentation.MIN_VALUE_ROLL, "0");
        
    }
    
    @Test
    public void testRollRepresentationNumber() {
        
        Assertions.assertEquals(RollRepresentation.NUMBER, "(\\b(^[0-9]|10$)\\b)");
        
    }
    
    @Test
    public void testRollRepresentationSpare() {
        
        Assertions.assertEquals(RollRepresentation.SPARE, "/");
        
    }
    
    @Test
    public void testRollRepresentationStrike() {
        
        Assertions.assertEquals(RollRepresentation.STRIKE, "X");
        
    }
    
    @Test
    public void testRollRepresentationZero() {
        
        Assertions.assertEquals(RollRepresentation.ZERO, "-");
        
    }
    
}
