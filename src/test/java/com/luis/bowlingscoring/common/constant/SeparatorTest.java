
package com.luis.bowlingscoring.common.constant;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SeparatorTest {
    
    public SeparatorTest() {
    }

    @Test
    public void testSeparatorExtension() {
        
        Assertions.assertEquals(Separator.EXTENSION_SEPARATOR, ".");
    }

    @Test
    public void testSeparatorMark() {
        
        Assertions.assertEquals(Separator.MARK, "\t");
    }
    
}
