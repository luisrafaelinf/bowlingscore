
package com.luis.bowlingscoring.common.constant;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RollAgrupationTest {
    
    public RollAgrupationTest() {
    }
    
    @Test
    public void testRollAgrupationMax() {
        
        Assertions.assertEquals(RollAgrupation.MAX, 3);
        
    }
    
    @Test
    public void testRollAgrupationMin() {
        
        Assertions.assertEquals(RollAgrupation.MIN, 1);
        
    }
    
}
