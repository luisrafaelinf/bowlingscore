package com.luis.bowlingscoring.common.validator;

import com.luis.bowlingscoring.common.fakedata.FakeRolls;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DefaultPlayerRollValidatorTest {

    public DefaultPlayerRollValidatorTest() {
    }

    @Test
    public void testInvalidRollsSize() {

        PlayerRollValidator instance = DefaultPlayerRollValidator.getInstance();
        boolean valid = instance.valid(FakeRolls.getRolls(1, "0"));

        Assertions.assertFalse(valid);
    }

    @Test
    public void testValidRollsSize() {

        PlayerRollValidator instance = DefaultPlayerRollValidator.getInstance();
        boolean valid = instance.valid(FakeRolls.getRolls(21, "X"));

        Assertions.assertTrue(valid);
    }

}
