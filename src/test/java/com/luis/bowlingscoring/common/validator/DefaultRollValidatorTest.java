package com.luis.bowlingscoring.common.validator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DefaultRollValidatorTest {

    public DefaultRollValidatorTest() {
    }

    @Test
    public void testValidRoll() {

        RollValidator instance = DefaultRollValidator.getInstance();
        boolean validScore = instance.isValidScore("X");

        Assertions.assertTrue(validScore);
    }

    @Test
    public void testValidStrike() {

        RollValidator instance = DefaultRollValidator.getInstance();
        boolean validScore = instance.isStrike("X");

        Assertions.assertTrue(validScore);
    }

    @Test
    public void testValidFault() {

        RollValidator instance = DefaultRollValidator.getInstance();
        boolean validScore = instance.isFault("F");

        Assertions.assertTrue(validScore);
    }

    @Test
    public void testValidSpare() {

        RollValidator instance = DefaultRollValidator.getInstance();
        boolean validScore = instance.isSpare("/");

        Assertions.assertTrue(validScore);
    }

    @Test
    public void testValidNumber() {

        RollValidator instance = DefaultRollValidator.getInstance();
        boolean validScore = instance.isScorePositiveNumber("6");

        Assertions.assertTrue(validScore);
    }

    @Test
    public void testInvalidNumber() {

        RollValidator instance = DefaultRollValidator.getInstance();
        boolean validScoreNegative = instance.isScorePositiveNumber("-6");
        boolean validScorePositive = instance.isScorePositiveNumber("11");

        Assertions.assertFalse(validScoreNegative);
        Assertions.assertFalse(validScorePositive);
    }

}
