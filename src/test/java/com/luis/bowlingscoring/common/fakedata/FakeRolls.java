

package com.luis.bowlingscoring.common.fakedata;

import com.luis.bowlingscoring.common.model.Roll;
import com.luis.bowlingscoring.common.validator.DefaultRollValidator;
import java.util.ArrayList;
import java.util.List;


public final class FakeRolls {
    
    private FakeRolls(){}
    
    public static List<Roll> getRolls(int amount, String roll) {
        
        List<Roll> rolls = new ArrayList<>();
        
        for (int i = 0; i < amount; i++) {
            rolls.add(Roll.getInstance(roll, DefaultRollValidator.getInstance()));
        }
        
        return rolls;
        
    }

}
