
package com.luis.bowlingscoring.filerepository.facade;

import com.luis.bowlingscoring.common.repository.ScoreRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FileScoreRepositoryFacadeTest {
    
    public FileScoreRepositoryFacadeTest() {
    }
    
    @Test
    public void testSomeMethod() {
        
        ScoreRepository fileScores = new FileScoreRepositoryFacade().get();
        
        Assertions.assertNotNull(fileScores);
        
    }
    
}
