package com.luis.bowlingscoring.filerepository.conection;

import com.luis.bowlingscoring.common.exception.EmptyFileException;
import com.luis.bowlingscoring.common.exception.UnprocessableFileException;
import com.luis.bowlingscoring.filerepository.validation.DefaultFileValidator;
import com.luis.bowlingscoring.filerepository.validation.FileValidator;
import com.luis.bowlingscoring.filerepository.validation.Formatt.DefaultFormatScoreInput;
import com.luis.bowlingscoring.filerepository.validation.file.DefaultInputFileValidator;
import java.io.IOException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DefaultLoadSourceTest {

    private final String EMPTY_FILE = "src/test/resources/empty.txt";
    private final String FILE_Player = "src/test/resources/one_player.txt";
    private final String FILE_UNPROCESSABLE = "src/test/resources/unprocessable.txt";

    public DefaultLoadSourceTest() {
    }

    @Test
    public void testLoadUnknowFile() {

        FileValidator fileValidator = DefaultFileValidator.getInstance(DefaultInputFileValidator.getInstance(), DefaultFormatScoreInput.getInstance());
        LoadSource source = DefaultLoadSource.getInstance();

        Exception exception = Assertions.assertThrows(IOException.class, () -> {
            source.readFile("", fileValidator);
        });

        String expectedMessage = "The file is incorrect";
        String message = exception.getLocalizedMessage();

        Assertions.assertEquals(expectedMessage, message);

    }

    @Test
    public void testLoadEmptyFile() {

        FileValidator fileValidator = DefaultFileValidator.getInstance(DefaultInputFileValidator.getInstance(), DefaultFormatScoreInput.getInstance());
        LoadSource source = DefaultLoadSource.getInstance();

        Exception exception = Assertions.assertThrows(EmptyFileException.class, () -> {
            source.readFile(EMPTY_FILE, fileValidator);
        });

        String expectedMessage = "The input file is empty. Use a file with player information score.";
        String message = exception.getLocalizedMessage();

        Assertions.assertEquals(expectedMessage, message);

    }

    @Test
    public void testLoadUnprocessable() {

        FileValidator fileValidator = DefaultFileValidator.getInstance(DefaultInputFileValidator.getInstance(), DefaultFormatScoreInput.getInstance());
        LoadSource source = DefaultLoadSource.getInstance();

        Exception exception = Assertions.assertThrows(UnprocessableFileException.class, () -> {
            source.readFile(FILE_UNPROCESSABLE, fileValidator);
        });

        String expectedMessage = "The file contains ivalid information. Please verify the information inside.";
        String message = exception.getLocalizedMessage();

        Assertions.assertEquals(expectedMessage, message);

    }

}
