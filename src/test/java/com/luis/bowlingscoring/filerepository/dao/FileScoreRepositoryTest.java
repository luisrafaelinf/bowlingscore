
package com.luis.bowlingscoring.filerepository.dao;

import com.luis.bowlingscoring.common.exception.EmptyFileException;
import com.luis.bowlingscoring.common.model.PlayerRoll;
import com.luis.bowlingscoring.common.repository.ScoreRepository;
import com.luis.bowlingscoring.filerepository.facade.FileScoreRepositoryFacade;
import com.luis.bowlingscoring.filerepository.model.FilePlayerQuery;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class FileScoreRepositoryTest {
    
    public FileScoreRepositoryTest() {
    }

    @Test
    public void testGetPlayerScores() throws IOException {
        
        ScoreRepository fileScores = new FileScoreRepositoryFacade().get();
        List<PlayerRoll> playerScores = fileScores.getPlayerScores(new FilePlayerQuery("src/test/resources/one_player.txt"));
        
        Assertions.assertFalse(playerScores.isEmpty());
        
    }

    @Test
    public void testGetPlayerScoresEmptyFile() throws IOException {
        
        ScoreRepository fileScores = new FileScoreRepositoryFacade().get();
        Exception exception = Assertions.assertThrows(EmptyFileException.class, () -> {
            fileScores.getPlayerScores(new FilePlayerQuery("src/test/resources/empty.txt"));
        });

        String expectedMessage = "The input file is empty. Use a file with player information score.";
        String message = exception.getLocalizedMessage();

        Assertions.assertEquals(expectedMessage, message);
        
    }
    
}
