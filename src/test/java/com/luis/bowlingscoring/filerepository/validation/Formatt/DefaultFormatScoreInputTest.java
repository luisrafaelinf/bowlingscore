
package com.luis.bowlingscoring.filerepository.validation.Formatt;

import com.luis.bowlingscoring.common.constant.Separator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DefaultFormatScoreInputTest {
    
    public DefaultFormatScoreInputTest() {
    }
    
    @Test
    public void testLineEmpty() {

        FormatScoreInput instance = DefaultFormatScoreInput.getInstance();
        
        boolean lineEmpty = instance.isLineEmpty("");
        boolean lineNotEmpty = instance.isLineEmpty("Luis");
        boolean lineNull = instance.isLineEmpty(null);
        
        Assertions.assertTrue(lineEmpty);
        Assertions.assertFalse(lineNotEmpty);
        Assertions.assertFalse(lineNull);
        
    }
    
    @Test
    public void testLinePlayerScore() {

        FormatScoreInput instance = DefaultFormatScoreInput.getInstance();
        
        boolean linePlayerScore = instance.isLinePlayerScore("Erick"+Separator.MARK+"10");
        boolean lineNotPlayerScore1 = instance.isLinePlayerScore("Erick"+Separator.MARK+"102");
        boolean lineNotPlayerScore2 = instance.isLinePlayerScore("Luis20");
        
        Assertions.assertTrue(linePlayerScore);
        Assertions.assertFalse(lineNotPlayerScore1);
        Assertions.assertFalse(lineNotPlayerScore2);
        
    }
    
    
    
}
