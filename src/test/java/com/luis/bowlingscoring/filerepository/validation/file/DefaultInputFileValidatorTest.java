
package com.luis.bowlingscoring.filerepository.validation.file;

import java.io.File;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DefaultInputFileValidatorTest {
    
    public DefaultInputFileValidatorTest() {
    }

    @Test
    public void testIsTextFile() {
        
        InputFileValidator instance = DefaultInputFileValidator.getInstance();
        
        boolean directory = instance.isTextFile(new File("src/"));
        boolean textFile = instance.isTextFile(new File("src/test/resources/one_player.txt"));
        boolean pdfFile = instance.isTextFile(new File("src/test/resources/pdf.pdf"));
        
        Assertions.assertFalse(directory);
        Assertions.assertTrue(textFile);
        Assertions.assertFalse(pdfFile);
    }
    
}
