package com.luis.bowlingscoring.filerepository.validation;

import com.luis.bowlingscoring.filerepository.validation.Formatt.DefaultFormatScoreInput;
import com.luis.bowlingscoring.filerepository.validation.file.DefaultInputFileValidator;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DefaultFileValidatorTest {

    private FileValidator instance = DefaultFileValidator.getInstance(
            DefaultInputFileValidator.getInstance(),
            DefaultFormatScoreInput.getInstance()
    );

    public DefaultFileValidatorTest() {
    }

    @Test
    public void testIsValidFile() {

        boolean validFile = instance.isValidFile(new File("src/test/resources/one_player.txt"));
        Assertions.assertTrue(validFile);

    }

    @Test
    public void testSomeMethod() throws IOException {

        List<String> allLines = Files.readAllLines(Paths.get("src/test/resources/one_player.txt"));
        List<String> allLinesBad = Files.readAllLines(Paths.get("src/test/resources/unprocessable.txt"));

        boolean validFile = instance.isProcessableFile(allLines);
        boolean validFileBad = instance.isProcessableFile(allLinesBad);

        Assertions.assertTrue(validFile);
        Assertions.assertFalse(validFileBad);

    }

}
