package com.luis.bowlingscoring.fakedata;

import com.luis.bowlingscoring.common.model.Roll;
import com.luis.bowlingscoring.common.validator.DefaultRollValidator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public final class FakeRolls {

    private FakeRolls() {
    }

    public static List<Roll> getRolls(int amount, String roll) {

        List<Roll> rolls = new ArrayList<>();

        for (int i = 0; i < amount; i++) {
            rolls.add(Roll.getInstance(roll, DefaultRollValidator.getInstance()));
        }

        return rolls;

    }

    public static List<Roll> getNormalGame() {

        return Arrays.asList("8","2","7","3","3","4","10","2","8","10","10","8","F","10","8","2","9")
                .stream()
                .map(s -> Roll.getInstance(s, DefaultRollValidator.getInstance()))
                .collect(Collectors.toList());
        
        
        

    }

}
