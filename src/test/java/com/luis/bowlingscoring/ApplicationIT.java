package com.luis.bowlingscoring;

import com.luis.bowlingscoring.common.model.PlayerQuery;
import com.luis.bowlingscoring.common.repository.ScoreRepository;
import com.luis.bowlingscoring.filerepository.facade.FileScoreRepositoryFacade;
import com.luis.bowlingscoring.filerepository.model.FilePlayerQuery;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ApplicationIT {

    private ScoreRepository fileRepository;

    public ApplicationIT() {
        fileRepository = new FileScoreRepositoryFacade().get();
    }

    @Test
    public void testGetScorePerfectGame() throws IOException {
        PlayerQuery path = new FilePlayerQuery("src/test/resources/perfect_scores.txt");
        List playerScores = fileRepository.getPlayerScores(path);

        Assertions.assertNotNull(playerScores);

    }

    @Test
    public void testGetScoreFouls() throws IOException {
        PlayerQuery path = new FilePlayerQuery("src/test/resources/fouls_score.txt");
        List playerScores = fileRepository.getPlayerScores(path);

        Assertions.assertNotNull(playerScores);

    }

    @Test
    public void testGetScoreOnePlayer() throws IOException {
        PlayerQuery path = new FilePlayerQuery("src/test/resources/one_player.txt");
        List playerScores = fileRepository.getPlayerScores(path);

        Assertions.assertNotNull(playerScores);

    }

    @Test
    public void testGetScoreTwoPlayer() throws IOException {
        PlayerQuery path = new FilePlayerQuery("src/test/resources/two_player.txt");
        List playerScores = fileRepository.getPlayerScores(path);

        Assertions.assertNotNull(playerScores);

    }

    @Test
    public void testGetScoreZero() throws IOException {
        PlayerQuery path = new FilePlayerQuery("src/test/resources/zero_score.txt");
        List playerScores = fileRepository.getPlayerScores(path);

        Assertions.assertNotNull(playerScores);

    }

}
