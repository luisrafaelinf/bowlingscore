

package com.luis.bowlingscoring.printer;

import com.luis.bowlingscoring.common.constant.RollRepresentation;
import com.luis.bowlingscoring.common.constant.FrameAgrupation;
import com.luis.bowlingscoring.common.constant.RollAgrupation;
import com.luis.bowlingscoring.common.model.Frame;
import com.luis.bowlingscoring.common.model.Pinfall;
import com.luis.bowlingscoring.common.model.Player;
import java.util.List;
import java.util.Objects;


public final class ConsolePrinter implements Printer {
    
    private ConsolePrinter(){}
    
    public static Printer getInstance() {
        return new ConsolePrinter();
    }

    @Override
    public void showData(List<Player> players) {
        
        showHeader();
        
        for (Player player : players) {
            
            showPlayerName(player.getName());
            showPinfall(player.getPinfall());
            showScoreFrame(player.getFrames());
            
        }
        
    }
    
    @Override
    public void showHeader() {
        
        System.out.print("Frame");
        
        for (int i = 1; i <= FrameAgrupation.TOTAL; i++) {
            System.out.print("\t\t"+i);
        }
        System.out.println("");
        
    }

    @Override
    public void showPlayerName(String playerName) {
        System.out.println(playerName);        
    }

    @Override
    public void showPinfall(List<Pinfall> pinfalls) {
        
        System.out.print("Pinfalls");
        
        int lastFrameIndex = pinfalls.size() - RollAgrupation.MAX;
        
        for (int i = 0; i < pinfalls.size(); i++) {
            
            String roll = pinfalls.get(i).getRoll();
            
            if (Objects.equals(roll, RollRepresentation.STRIKE) 
                    && i < lastFrameIndex) {
                System.out.print("\t\t"+roll);
            } else {
                System.out.print("\t"+roll);
            }
            
        }
        
        
        System.out.println("");
    }

    @Override
    public void showScoreFrame(List<Frame> frames) {
        
        System.out.print("Score");
        
        for (Frame frame : frames) {
            System.out.print("\t\t"+frame.getAmount());
        }
        System.out.println("");
        
    }

}
