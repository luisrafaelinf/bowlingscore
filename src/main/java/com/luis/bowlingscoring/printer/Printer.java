

package com.luis.bowlingscoring.printer;

import com.luis.bowlingscoring.common.model.Frame;
import com.luis.bowlingscoring.common.model.Pinfall;
import com.luis.bowlingscoring.common.model.Player;
import java.util.List;


public interface Printer {

    public void showData(List<Player> players);
    
    public void showHeader();
    public void showPlayerName(String playerName);
    public void showPinfall(List<Pinfall> pinfalls);
    public void showScoreFrame(List<Frame> frames);
    
}
