

package com.luis.bowlingscoring.filerepository.conection;

import com.luis.bowlingscoring.common.exception.EmptyFileException;
import java.io.IOException;
import java.util.List;
import com.luis.bowlingscoring.filerepository.validation.FileValidator;


public interface LoadSource {
    
    public List<String> readFile(String inputPath, FileValidator validatorFile) throws IOException, EmptyFileException;
        

}
