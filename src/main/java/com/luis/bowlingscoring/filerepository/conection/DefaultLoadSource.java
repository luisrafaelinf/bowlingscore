

package com.luis.bowlingscoring.filerepository.conection;

import com.luis.bowlingscoring.common.exception.EmptyFileException;
import com.luis.bowlingscoring.common.exception.UnprocessableFileException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;
import com.luis.bowlingscoring.filerepository.validation.FileValidator;


public class DefaultLoadSource implements LoadSource {
    
    private DefaultLoadSource() {}
    
    public static LoadSource getInstance() {
        return new DefaultLoadSource();
    }    
    
    @Override
    public List<String> readFile(String inputPath, FileValidator validatorFile) throws IOException{
        
            File file = new File(inputPath);
           
            if (!validatorFile.isValidFile(file)) {
                throw new IOException("The file is incorrect");
            }
            
            List<String> lines = getLines(file);
            
            if (lines.isEmpty()) {
                throw new EmptyFileException("The input file is empty. Use a file with player information score.");
            }
            
            if (!validatorFile.isProcessableFile(lines)) {
                throw new UnprocessableFileException("The file contains ivalid information. Please verify the information inside.");
            }
        
            return lines;
    
    }

    private List<String> getLines(File file) throws IOException {
        
        InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(file));
        
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        List<String> collectLines = bufferedReader.lines().collect(Collectors.toList());
        
        return collectLines;
    }
    

}
