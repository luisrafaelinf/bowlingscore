

package com.luis.bowlingscoring.filerepository.model;

import com.luis.bowlingscoring.common.model.PlayerQuery;


public class FilePlayerQuery implements PlayerQuery {

    private final String path;

    public FilePlayerQuery(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
    
}
