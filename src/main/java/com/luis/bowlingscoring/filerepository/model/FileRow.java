

package com.luis.bowlingscoring.filerepository.model;


public final class FileRow {

    private final String playerName;
    private final String roll;
    
    private FileRow(String playerName, String roll) {
        this.playerName = playerName;
        this.roll = roll;
    }
    
    public static FileRow getinstance(String playerName, String roll) {
        return new FileRow(playerName, roll);
    }

    public String getPlayerName() {
        return playerName;
    }
    
    public String getRoll() {
        return roll;
    }
    
}
