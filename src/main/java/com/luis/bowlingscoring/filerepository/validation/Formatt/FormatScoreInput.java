

package com.luis.bowlingscoring.filerepository.validation.Formatt;

public interface FormatScoreInput {
    
    public boolean isLineEmpty(String line);
    
    public boolean isLinePlayerScore(String line);
    
}
