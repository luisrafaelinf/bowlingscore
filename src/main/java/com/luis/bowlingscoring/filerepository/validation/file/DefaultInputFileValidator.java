

package com.luis.bowlingscoring.filerepository.validation.file;

import com.luis.bowlingscoring.common.constant.Separator;
import java.io.File;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;


public final class DefaultInputFileValidator implements InputFileValidator {
    
    private static final String EXTENSION = "txt";
        
    private DefaultInputFileValidator(){}
    
    public static InputFileValidator getInstance() {
        return new DefaultInputFileValidator();
    }

    @Override
    public boolean isTextFile(File file) {
        
        return file.isFile()
                && isTxtExtension(file);   
    }
    
    private boolean isTxtExtension(File file) {
        
        return Optional.ofNullable(file.getName())
                .filter(containDot())
                .map(getExtension())
                .filter(isTxt())
                .isPresent();
        
    }

    private Predicate<String> containDot() {
        return (name) -> name.contains(Separator.EXTENSION_SEPARATOR);
    }
    
    private Function<String, String> getExtension() {
        return (name) -> name.substring(name.lastIndexOf(Separator.EXTENSION_SEPARATOR) + 1);
    }

    private Predicate<String> isTxt() {
        return (extension) -> Objects.equals(extension, EXTENSION);
    }
}
