

package com.luis.bowlingscoring.filerepository.validation;

import java.io.File;
import java.util.List;


public interface FileValidator {

    public boolean isValidFile(File file);

    public boolean isProcessableFile(List<String> linesFromFile);
    
}
