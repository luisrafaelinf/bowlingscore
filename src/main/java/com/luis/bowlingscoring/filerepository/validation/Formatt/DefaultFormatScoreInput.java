

package com.luis.bowlingscoring.filerepository.validation.Formatt;

import com.luis.bowlingscoring.common.constant.Separator;
import java.util.Objects;


public final class DefaultFormatScoreInput implements FormatScoreInput{
    
    private DefaultFormatScoreInput() {}
    
    public static FormatScoreInput getInstance() {
        return new DefaultFormatScoreInput();
    }
    
    @Override
    public boolean isLinePlayerScore(String line) {
        
        final String[] values = line.split(Separator.MARK);
        
        return (values.length == 2) 
                && (values[1].trim().length() <= 2);
        
    }

    @Override
    public boolean isLineEmpty(String line) {
                
        return Objects.nonNull(line) 
                && line.isEmpty();
    }

}
