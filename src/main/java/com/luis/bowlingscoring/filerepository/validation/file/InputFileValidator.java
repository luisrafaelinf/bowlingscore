

package com.luis.bowlingscoring.filerepository.validation.file;

import java.io.File;


public interface InputFileValidator  {
    
    public boolean isTextFile(File file);
    
}
