package com.luis.bowlingscoring.filerepository.validation;

import com.luis.bowlingscoring.filerepository.validation.file.InputFileValidator;
import java.io.File;
import java.util.List;
import com.luis.bowlingscoring.filerepository.validation.Formatt.FormatScoreInput;

public final class DefaultFileValidator implements FileValidator {

    private InputFileValidator inputFileValidator;
    private FormatScoreInput formattScoreInput;

    private DefaultFileValidator(InputFileValidator inputFileValidator, FormatScoreInput formattScoreInput) {
        this.inputFileValidator = inputFileValidator;
        this.formattScoreInput = formattScoreInput;
    }
    
    public static FileValidator getInstance(InputFileValidator inputFileValidator, FormatScoreInput formattScoreInput) {
        return new DefaultFileValidator(inputFileValidator, formattScoreInput);
    }

    @Override
    public boolean isValidFile(File file) {

        return inputFileValidator.isTextFile(file);
    }

    @Override
    public boolean isProcessableFile(List<String> linesFromFile) {

        return linesFromFile.stream()
                .filter(this::isNonProcessableLine)
                .count() == 0;

    }

    private boolean isNonProcessableLine(String line) {
        
        return !(!formattScoreInput.isLineEmpty(line)
                && formattScoreInput.isLinePlayerScore(line));
    }

}
