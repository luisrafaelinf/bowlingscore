package com.luis.bowlingscoring.filerepository.dao;

import com.luis.bowlingscoring.common.repository.ScoreRepository;
import com.luis.bowlingscoring.common.constant.Separator;
import com.luis.bowlingscoring.common.exception.EmptyFileException;
import com.luis.bowlingscoring.common.model.PlayerRoll;
import com.luis.bowlingscoring.common.model.Roll;
import com.luis.bowlingscoring.common.validator.PlayerRollValidator;
import java.util.List;
import com.luis.bowlingscoring.filerepository.conection.LoadSource;
import com.luis.bowlingscoring.filerepository.model.FilePlayerQuery;
import com.luis.bowlingscoring.filerepository.model.FileRow;
import java.util.ArrayList;
import java.util.function.Function;
import java.util.stream.Collectors;
import com.luis.bowlingscoring.filerepository.validation.FileValidator;
import com.luis.bowlingscoring.common.validator.RollValidator;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileScoreRepository implements ScoreRepository<FilePlayerQuery> {

    private final LoadSource inputFile;
    private final FileValidator validatorFile;
    private final RollValidator scoreValidator;
    private final PlayerRollValidator playerRollValidator;

    private FileScoreRepository(LoadSource inputFile, FileValidator validatorFile, RollValidator scoreValidator, PlayerRollValidator playerRollValidator) {
        this.inputFile = inputFile;
        this.validatorFile = validatorFile;
        this.scoreValidator = scoreValidator;
        this.playerRollValidator = playerRollValidator;
    }

    public static FileScoreRepository getInstance(LoadSource inputFile, FileValidator validatorFile, RollValidator scoreValidator, PlayerRollValidator playerRollValidator) {

        return new FileScoreRepository(inputFile, validatorFile, scoreValidator, playerRollValidator);
    }

    @Override
    public List<PlayerRoll> getPlayerScores(FilePlayerQuery query) {

        try {
            List<String> readrolls = new ArrayList<>();
            
            readrolls = inputFile.readFile(query.getPath(), validatorFile);
            
            return map(readrolls);
        } catch (IOException | EmptyFileException ex) {
            throw new EmptyFileException(ex.getMessage());
        }

    }

    private List<PlayerRoll> map(List<String> lines) {

        return lines.stream()
                
                .map(mapScore())
                .collect(Collectors.groupingBy(FileRow::getPlayerName, LinkedHashMap::new,
                        Collectors.mapping(mapToRollEntity(), Collectors.toList())
                ))
                .entrySet()
                .stream()
                .map(e -> PlayerRoll.getinstance(e.getKey(), e.getValue(), playerRollValidator))
                .collect(Collectors.toList());

    }

    private Function<String, FileRow> mapScore() {

        return (String line) -> {

            String[] gameScoreSplit = line.split(Separator.MARK);

            return FileRow.getinstance(gameScoreSplit[0], gameScoreSplit[1]);
        };
    }

    private Function<FileRow, Roll> mapToRollEntity() {
        return (score) -> Roll.getInstance(score.getRoll(), scoreValidator);
    }

}
