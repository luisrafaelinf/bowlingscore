package com.luis.bowlingscoring.filerepository.facade;

import com.luis.bowlingscoring.common.repository.ScoreRepository;
import com.luis.bowlingscoring.common.repository.ScoreRepositoryFacade;
import com.luis.bowlingscoring.common.validator.DefaultPlayerRollValidator;
import com.luis.bowlingscoring.common.validator.DefaultRollValidator;
import com.luis.bowlingscoring.filerepository.conection.DefaultLoadSource;
import com.luis.bowlingscoring.filerepository.dao.FileScoreRepository;
import com.luis.bowlingscoring.filerepository.validation.DefaultFileValidator;
import com.luis.bowlingscoring.filerepository.validation.Formatt.DefaultFormatScoreInput;
import com.luis.bowlingscoring.filerepository.validation.file.DefaultInputFileValidator;
import com.luis.bowlingscoring.filerepository.validation.file.InputFileValidator;
import com.luis.bowlingscoring.filerepository.validation.Formatt.FormatScoreInput;

public final class FileScoreRepositoryFacade implements ScoreRepositoryFacade{

    @Override
    public ScoreRepository get() {

        InputFileValidator inputFileValidator = DefaultInputFileValidator.getInstance();
        FormatScoreInput formattScoreInput = DefaultFormatScoreInput.getInstance();

        return FileScoreRepository.getInstance(
                DefaultLoadSource.getInstance(),
                DefaultFileValidator.getInstance(inputFileValidator, formattScoreInput),
                DefaultRollValidator.getInstance(),
                DefaultPlayerRollValidator.getInstance()
        );

    }

}
