package com.luis.bowlingscoring;

import com.luis.bowlingscoring.common.model.PlayerRoll;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;
import com.luis.bowlingscoring.common.model.Player;
import com.luis.bowlingscoring.common.repository.ScoreRepository;
import com.luis.bowlingscoring.filerepository.facade.FileScoreRepositoryFacade;
import com.luis.bowlingscoring.filerepository.model.FilePlayerQuery;
import com.luis.bowlingscoring.common.model.PlayerQuery;
import com.luis.bowlingscoring.common.exception.EmptyFileException;
import com.luis.bowlingscoring.common.exception.InvalidRollException;
import com.luis.bowlingscoring.common.exception.UnprocessableFileException;
import com.luis.bowlingscoring.common.repository.ScoreRepositoryFacade;
import com.luis.bowlingscoring.printer.ConsolePrinter;
import com.luis.bowlingscoring.printer.Printer;
import com.luis.bowlingscoring.scoreprocessor.DefaultGameScoreProcessor;
import com.luis.bowlingscoring.scoreprocessor.GameScoreProcessor;
import java.io.IOException;

public class Application {

    private static final Logger LOG = Logger.getLogger(Application.class.getName());

    public static void main(String[] args) throws IOException {

        if (args.length > 1) {
            LOG.log(Level.SEVERE, "You need provide a file with the player(s) and scores. \n"
                    + "Please provide a correct input file.");
            System.exit(0);
        }

        PlayerQuery path = getPath(args[0]);

        try {

            ScoreRepositoryFacade scoreRepositoryFacade = new FileScoreRepositoryFacade();
            
            ScoreRepository fileRepository = scoreRepositoryFacade.get();
            List<PlayerRoll> dataMapped = fileRepository.getPlayerScores(path);

            GameScoreProcessor scoreProcessed = DefaultGameScoreProcessor.getInstance();
            List<Player> playersProcessed = scoreProcessed.process(dataMapped);

            Printer printer = ConsolePrinter.getInstance();
            printer.showData(playersProcessed);

        } catch ( InvalidRollException ex) {
            LOG.log(Level.SEVERE, ex.getMessage());
        } catch (EmptyFileException | UnprocessableFileException ex) {
            LOG.log(Level.SEVERE, ex.getMessage());
        } catch (NullPointerException | ArrayIndexOutOfBoundsException ex) {
            LOG.log(Level.SEVERE, "Missing rolls. Please review the input again.");
        } 

    }

    private static PlayerQuery getPath(String path) {
        return new FilePlayerQuery(path);
    }

}
