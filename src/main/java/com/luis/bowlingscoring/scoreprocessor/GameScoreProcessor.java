

package com.luis.bowlingscoring.scoreprocessor;

import com.luis.bowlingscoring.common.model.PlayerRoll;
import com.luis.bowlingscoring.common.model.Roll;
import com.luis.bowlingscoring.common.model.Frame;
import com.luis.bowlingscoring.common.model.Pinfall;
import com.luis.bowlingscoring.common.model.Player;
import java.util.List;


public interface GameScoreProcessor {
    
    public List<Player> process(List<PlayerRoll> gameScores);
        
    public List<Pinfall> processPinFall(List<Roll> rolls);
    
    public List<Frame> processScoreFrame(List<Roll> rolls);    

}
