package com.luis.bowlingscoring.scoreprocessor;

import com.luis.bowlingscoring.common.constant.FrameAgrupation;
import com.luis.bowlingscoring.common.constant.RollAgrupation;
import com.luis.bowlingscoring.common.model.Frame;
import com.luis.bowlingscoring.common.model.Pinfall;
import com.luis.bowlingscoring.common.model.Player;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.luis.bowlingscoring.common.constant.RollRepresentation;
import com.luis.bowlingscoring.common.model.PlayerRoll;
import com.luis.bowlingscoring.common.model.Roll;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class DefaultGameScoreProcessor implements GameScoreProcessor {
    
    final int maxRollValue;

    private DefaultGameScoreProcessor() {
        
        maxRollValue = Integer.parseInt(RollRepresentation.MAX_VALUE_ROLL);
    }

    public static GameScoreProcessor getInstance() {
        return new DefaultGameScoreProcessor();
    }

    @Override
    public List<Player> process(List<PlayerRoll> gameScores) throws NullPointerException, ArrayIndexOutOfBoundsException {

        List<Player> players = new ArrayList<>();

        for (PlayerRoll gameScore : gameScores) {

            List<Roll> rolls = gameScore.getRolls();

            Player player = new Player(gameScore.getPlayerName());

            player.setPinfall(processPinFall(rolls));
            player.setFrames(processScoreFrame(rolls));
            players.add(player);

        }
        
        return players;

    }

    @Override
    public List<Pinfall> processPinFall(List<Roll> rolls) throws NullPointerException, ArrayIndexOutOfBoundsException{

        final List<Pinfall> pinfalls = new ArrayList<>();

        int lastFrameIndex = rolls.size() - RollAgrupation.MAX;
        int index = 0;

        for (int i = 0; i < rolls.size(); i++) {

            Roll roll = rolls.get(i);
            String value = roll.value();

            String valueToPrint = valueMask(value);

            if (i <= lastFrameIndex
                    && Objects.equals(value, maxRollValue)) {
                
                pinfalls.add(new Pinfall(valueToPrint));
                index += 2;
                
            } else {

                if (!value.equals(RollRepresentation.FAULT) 
                        && (index % 2) != 0) {

                    int firstValue = rolls.get(i - 1).getScore();
                    int secondValue = roll.getScore();
                    if ((firstValue + secondValue) == maxRollValue
                            && !value.equals(RollRepresentation.MAX_VALUE_ROLL)) {
                        valueToPrint = RollRepresentation.SPARE;
                    }

                }
                
                index += 1;
                pinfalls.add(new Pinfall(valueToPrint));
            }

        }

        return pinfalls;

    }

    @Override
    public List<Frame> processScoreFrame(List<Roll> rolls) throws NullPointerException, ArrayIndexOutOfBoundsException {

        final Integer[] frames = new Integer[FrameAgrupation.TOTAL];
        
        int totalFrame = 0;
        int indexFrame = 0;
        int iterator = 0;

        do {

            int value = rolls.get(iterator).getScore();

            if (value == maxRollValue) {

                processingStrike(rolls, iterator, frames, indexFrame, totalFrame);

                totalFrame = frames[indexFrame];
                iterator += 1;
            
            } else if (value + rolls.get(iterator + 1).getScore() == maxRollValue) {

                processingSpare(rolls, iterator, frames, indexFrame, totalFrame);
                
                totalFrame = frames[indexFrame];
                iterator += 2;
                
            } else {
                
                frames[indexFrame] = totalFrame + value + rolls.get(iterator + 1).getScore();
                
                totalFrame = frames[indexFrame];
                iterator += 2;
            }

            indexFrame++;

        } while (iterator < rolls.size() - 2);

        return Stream.of(frames)
                .map(Frame::new)
                .collect(Collectors.toList());

    }

    private void processingSpare(List<Roll> rolls, int iterator, final Integer[] frames, int indexFrame, int totalFrame) throws NullPointerException {
        
        
        
        if (rolls.get(iterator + 2).getScore() == maxRollValue) {
            
            frames[indexFrame] = totalFrame + maxRollValue * 2;
            
        } else {
            
            frames[indexFrame] = totalFrame + maxRollValue + rolls.get(iterator + 2).getScore();
        }
    }

    private void processingStrike(List<Roll> rolls, int iterator, final Integer[] frames, int indexFrame, int totalFrame) throws NullPointerException {
        
        if (rolls.get(iterator + 1).getScore() == maxRollValue
                && rolls.get(iterator + 2).getScore() == maxRollValue) {
            
            frames[indexFrame] = totalFrame + maxRollValue * 3;
            
        } else if (rolls.get(iterator + 1).getScore() == maxRollValue) {
            
            frames[indexFrame] = totalFrame + (maxRollValue * 2) + rolls.get(iterator + 2).getScore();
            
        } else {
            
            frames[indexFrame] = totalFrame + maxRollValue + rolls.get(iterator + 1).getScore() + rolls.get(iterator + 2).getScore();
            
        }
    }

    private String valueMask(String roll) {

        switch (roll) {

            case RollRepresentation.MAX_VALUE_ROLL:
                return RollRepresentation.STRIKE;

            case RollRepresentation.MIN_VALUE_ROLL:
                return RollRepresentation.ZERO;

            default:
                return roll;
        }

    }

}
