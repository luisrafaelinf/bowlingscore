

package com.luis.bowlingscoring.common.validator;


public interface RollValidator {
    
    public boolean isScorePositiveNumber(String score);
    
    public boolean isStrike(String score);

    public boolean isSpare(String score);
    
    public boolean isFault(String score);
    
    public boolean isValidScore(String score);

}
