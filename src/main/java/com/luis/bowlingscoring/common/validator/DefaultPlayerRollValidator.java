package com.luis.bowlingscoring.common.validator;

import com.luis.bowlingscoring.common.constant.RollAgrupation;
import com.luis.bowlingscoring.common.constant.RollRepresentation;
import com.luis.bowlingscoring.common.model.Roll;
import java.util.List;
import java.util.Objects;

public final class DefaultPlayerRollValidator implements PlayerRollValidator {

    private DefaultPlayerRollValidator() {}

    public static PlayerRollValidator getInstance() {
        return new DefaultPlayerRollValidator();
    }

    @Override
    public boolean valid(List<Roll> playerRolls) {

        boolean lenght = playerRolls.size() >= RollRepresentation.MIN_ROLLS
                && playerRolls.size() <= RollRepresentation.MAX_ROLLS;

        boolean total = isTotalRollsValid(playerRolls);

        return lenght || total;
    }

    private boolean isTotalRollsValid(List<Roll> rolls) {

        int rollsCounted = rolls
                .stream()
                .mapToInt(r -> Objects.equals(r.value(), RollRepresentation.MAX_VALUE_ROLL) ? 2 : 1)
                .sum();

        int minimalUp = RollRepresentation.MAX_ROLLS - RollAgrupation.MAX;

        return (rollsCounted >= minimalUp)
                && (rollsCounted <= RollRepresentation.MAX_ROLLS);
    }

}
