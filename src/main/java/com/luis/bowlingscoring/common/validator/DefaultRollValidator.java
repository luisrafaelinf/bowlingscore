

package com.luis.bowlingscoring.common.validator;

import com.luis.bowlingscoring.common.constant.RollRepresentation;
import java.util.Objects;
import java.util.regex.Pattern;


public final class DefaultRollValidator implements RollValidator {
    
    private final Pattern pattern; 
    
    private DefaultRollValidator(){
        pattern = Pattern.compile(RollRepresentation.NUMBER);
    }
    
    public static RollValidator getInstance() {
        return new DefaultRollValidator();
    }

    @Override
    public boolean isValidScore(String score) {

        return isFault(score)
                || isSpare(score)
                || isStrike(score)
                || isScorePositiveNumber(score);
        
    }
    
    @Override
    public boolean isScorePositiveNumber(String score) {
        return pattern.matcher(score).find();
    }

    @Override
    public boolean isStrike(String score) {
        return Objects.equals(score,RollRepresentation.STRIKE);
    }

    @Override
    public boolean isSpare(String score) {
        return Objects.equals(score,RollRepresentation.SPARE);
    }

    @Override
    public boolean isFault(String score) {
        return Objects.equals(score,RollRepresentation.FAULT);
    }

}
