

package com.luis.bowlingscoring.common.validator;

import com.luis.bowlingscoring.common.model.Roll;
import java.util.List;


public interface PlayerRollValidator {

     public boolean valid(List<Roll> playerRolls);
}
