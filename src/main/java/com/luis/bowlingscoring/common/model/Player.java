

package com.luis.bowlingscoring.common.model;

import java.util.List;


public final class Player {
    
    private String name;
    private List<Pinfall> pinfall;
    private List<Frame> frames;

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Pinfall> getPinfall() {
        return pinfall;
    }

    public void setPinfall(List<Pinfall> pinfall) {
        this.pinfall = pinfall;
    }

    public List<Frame> getFrames() {
        return frames;
    }

    public void setFrames(List<Frame> frames) {
        this.frames = frames;
    }
    
}
