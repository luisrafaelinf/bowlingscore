

package com.luis.bowlingscoring.common.model;

import com.luis.bowlingscoring.common.exception.IllegalRollException;
import java.util.List;
import com.luis.bowlingscoring.common.validator.PlayerRollValidator;
import java.util.Objects;

public final class PlayerRoll {

    private final String playerName;
    private final List<Roll> rolls;
    
    private PlayerRoll(String playerName, List<Roll> rolls) {
        
        
        this.playerName = playerName;
        this.rolls = rolls;
    }
    
    public static PlayerRoll getinstance(String playerName, List<Roll> rolls, PlayerRollValidator validator) {
        
        Objects.requireNonNull(playerName, "Name of players must not be null");
        
        final boolean isListTotalRollsInvalid = validator.valid(rolls);

        if (!isListTotalRollsInvalid) {
            
            throw new IllegalRollException("The total of rolls is not valid. "
                    + "Please verify the file and fill it correctly.");
        }
        
        return new PlayerRoll(playerName, rolls);
        
        
    }

    public String getPlayerName() {
        return playerName;
    }
    
    public List<Roll> getRolls() {
        return rolls;
    }
    
}
