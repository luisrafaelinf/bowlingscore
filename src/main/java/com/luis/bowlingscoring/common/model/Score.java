

package com.luis.bowlingscoring.common.model;


public interface Score {
    
    public String value();

    public int getScore();
    
    public void setIndex(int index);
    
    public int index();
    
}
