

package com.luis.bowlingscoring.common.model;


public final class Pinfall {
    
    private String roll;

    public Pinfall(String roll) {
        this.roll = roll;
    }

    public String getRoll() {
        return roll;
    }
    
}
