

package com.luis.bowlingscoring.common.model;


public final class Frame {

    private int amount;

    public Frame(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }
    
}
