package com.luis.bowlingscoring.common.model;

import com.luis.bowlingscoring.common.constant.RollRepresentation;
import com.luis.bowlingscoring.common.exception.InvalidRollException;
import com.luis.bowlingscoring.common.validator.RollValidator;
import java.util.Objects;

public class Roll implements Score {

    private String value;
    private int index;

    private Roll(String value) {
        this.value = value;
    }

    public static Roll getInstance(String score, RollValidator scoreValidator) {
        Objects.requireNonNull(score, "Score must not be null");

        if (scoreValidator.isValidScore(score)) {
            return new Roll(score);
        }

        throw new InvalidRollException(String.format("Roll score %s invalid. Please verify again.", score));
        
//         Logger.getLogger(Roll.class.getName()).info( String.format("Roll score %s invalid. Please verify again.", score));
//        System.exit(0);
//        return null; 
        
    }

    @Override
    public String value() {
        return value;
    }

    @Override
    public int getScore() {
        switch (value()) {

            case RollRepresentation.FAULT: {
                return 0;
            }
            case RollRepresentation.STRIKE: {
                return 10;
            }
            default:
                return Integer.parseInt(value());

        }
    }

    @Override
    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public int index() {
        return this.index;
    }
}
