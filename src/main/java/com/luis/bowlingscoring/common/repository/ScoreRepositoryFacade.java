

package com.luis.bowlingscoring.common.repository;


public interface ScoreRepositoryFacade {
    
    public ScoreRepository get();

}
