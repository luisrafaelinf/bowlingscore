

package com.luis.bowlingscoring.common.repository;

import com.luis.bowlingscoring.common.model.PlayerQuery;
import com.luis.bowlingscoring.common.model.PlayerRoll;
import java.util.List;


public interface ScoreRepository<T extends PlayerQuery> {

    public List<PlayerRoll> getPlayerScores(T query);
    
}
