

package com.luis.bowlingscoring.common.exception;


public final class InvalidRollException extends RuntimeException {

    public InvalidRollException(String message) {
        super(message);
    }

}
