

package com.luis.bowlingscoring.common.exception;


public final class IllegalRollException extends RuntimeException {

    public IllegalRollException(String message) {
        super(message);
    }

}
