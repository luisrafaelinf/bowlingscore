

package com.luis.bowlingscoring.common.exception;


public final class UnprocessableFileException extends RuntimeException {

    public UnprocessableFileException(String message) {
        super(message);
    }

}
