

package com.luis.bowlingscoring.common.exception;


public final class EmptyFileException extends RuntimeException {

    public EmptyFileException(String message) {
        super(message);
    }

}
