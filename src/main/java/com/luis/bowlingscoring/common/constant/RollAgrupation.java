

package com.luis.bowlingscoring.common.constant;


public final class RollAgrupation {

    public static final int MIN = 1;
    public static final int MAX = 3;
    
    private RollAgrupation(){}
}
