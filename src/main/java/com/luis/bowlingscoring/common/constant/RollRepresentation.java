

package com.luis.bowlingscoring.common.constant;


public final class RollRepresentation {
 
    public static final String NUMBER = "(\\b(^[0-9]|10$)\\b)";
    public static final String STRIKE = "X";
    public static final String SPARE = "/";
    public static final String FAULT = "F";
    public static final String ZERO = "-";
    public static final String MAX_VALUE_ROLL = "10";
    public static final String MIN_VALUE_ROLL = "0";
    public static final int MAX_ROLLS = 24;
    public static final int MIN_ROLLS = 12;
    
    private RollRepresentation() {}
    
}
