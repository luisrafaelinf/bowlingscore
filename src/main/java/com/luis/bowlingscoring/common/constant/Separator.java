

package com.luis.bowlingscoring.common.constant;


public final class Separator {

    public static final String MARK = "\t";
    public static final String EXTENSION_SEPARATOR = ".";
    
    private Separator(){}
    
}
